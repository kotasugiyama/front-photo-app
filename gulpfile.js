var gulp = require('gulp');
var loadPlugins = require('gulp-load-plugins');
var plugin = loadPlugins();
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');

// コンパイル
gulp.task('compile', function(callback) {
    return runSequence(
        ['sass', 'ejs', 'images', 'js'],
        'min',
        callback
    );
});

// SASS
gulp.task('sass', function() {
    return gulp.src(['src/css/**/*.scss'])
        // app.scssを読みこまないので。。
        .pipe(plugin.cached(plugin.sass))
        .pipe(plugin.using())
        .pipe(plugin.sourcemaps.init())
        .pipe(plugin.csscomb())
        .pipe(plugin.sass({outputStyle: 'expanded'}))
        .pipe(plugin.sourcemaps.write("maps/"))
        .pipe(gulp.dest('dest/css'));
});

// appのみ
gulp.task('sass:app', function() {
    return gulp.src(['src/css/*.scss'])
        .pipe(plugin.using())
        .pipe(plugin.plumber())
        .pipe(plugin.sourcemaps.init())
        .pipe(plugin.csscomb())
        .pipe(plugin.sass({outputStyle: 'expanded'}).on('error', plugin.sass.logError))
        .pipe(plugin.sourcemaps.write("maps/"))
        .pipe(gulp.dest('dest/css'));
});

// js
gulp.task('js', function() {
    return gulp.src(['src/js/**/*'])
        .pipe(plugin.cached('loadPlugins'))
        .pipe(gulp.dest('dest/js'));
});

// images
gulp.task('images', function() {
    return gulp.src(['src/?(img|images)/**/*.?(jpg|png|gif)'])
        .pipe(plugin.cached('loadPlugins'))
        .pipe(plugin.imagemin())
        .pipe(gulp.dest('dest/'));
});

// 結合タスク
gulp.task('min', function() {
    return gulp.src(['src/css/**/reset.css', 'src/css/**/base.css', 'src/css/**/*.css'])
        .pipe(plugin.cached('loadPlugins'))
        .pipe(plugin.sourcemaps.init())
        .pipe(plugin.concat('all.css'))
        .pipe(plugin.minifyCss())
        .pipe(plugin.rename({extname: '.min.css'}))
        .pipe(gulp.dest('dest/css'));
});

//サーバ起動
gulp.task('server', function() {
    return browserSync({
        //startPath: "/front/pc/html/index.html",
        oepn: false,
        server: {
            baseDir: 'dest/'
        }
    });
});

//ejsコンパイル
gulp.task('ejs', function() {
    return gulp.src(["src/html/**/*.ejs",'!' + "src/html/**/_*.ejs"])
        //.pipe(plugin.cached('loadPlugins'))
        .pipe(plugin.using())
        .pipe(plugin.plumber({
            errorHandler: function(err) {
                console.log(err.plugin);
                console.log(err.message);
                this.emit('end');
            }
        }))
        .pipe(plugin.ejs({
            data:{
                rootPathPC: __dirname + '/src/pc/html/',
                rootPathSP: __dirname + '/src/sp/html/'
            }
        }).on('error', function (err) {
            console.log(err);
        }))
        .pipe(plugin.rename({extname: ".html"}))
        .pipe(gulp.dest("dest/"));
});
gulp.task('ejs:cached', function() {
    return gulp.src(["src/html/**/*.ejs",'!' + "src/html/**/_*.ejs"])
        .pipe(plugin.cached('loadPlugins'))
        .pipe(plugin.using())
        .pipe(plugin.plumber({
            errorHandler: function(err) {
                console.log(err.plugin);
                console.log(err.message);
                this.emit('end');
            }
        }))
        .pipe(plugin.ejs({
            data:{
                rootPathPC: __dirname + '/src/pc/html/',
                rootPathSP: __dirname + '/src/sp/html/'
            }
        }).on('error', function (err) {
            console.log(err);
        }))
        .pipe(plugin.rename({extname: ".html"}))
        .pipe(gulp.dest("dest/"));
});
// watch
gulp.task('watch', ['server'], function() {
        gulp.watch(['src/css/**/*.scss'], ['watch:sass']);
        gulp.watch(['src/html/**/*.ejs', '!src/html/**/_*.ejs'], ['watch:ejs:cached']);
        gulp.watch(['!src/html/**/*.ejs', 'src/html/**/_*.ejs'], ['watch:ejs']);
        gulp.watch(['src/js/**/*.js'], ['watch:js']);
});

// watch用タスク
gulp.task('watch:sass', function(callback) {
    return runSequence(
        'sass:app',
        'bsReload',
        callback
    );
});
gulp.task('watch:js', function(callback) {
    return runSequence(
        'js',
        'bsReload',
        callback
    );
});
gulp.task('watch:ejs', function(callback) {
    return runSequence(
        'ejs',
        'bsReload',
        callback
    );
});
gulp.task('watch:ejs:cached', function(callback) {
    return runSequence(
        'ejs:cached',
        'bsReload',
        callback
    );
});
gulp.task('bsReload', function() {
    browserSync.reload();
});